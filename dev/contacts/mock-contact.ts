import {Contact} from "./contact";

export const CONTACTS: Contact[] = [{firstName: "Dung", lastName: "Pham", age: 1, email: "Dung@gmail.com"},
									{firstName: "Dung 2", lastName: "Pham 2", age: 2, email: "Dung2@gmail.com"},
									{firstName: "Dung 3", lastName: "Pham 3", age: 3, email: "Dung3@gmail.com"}];