import {Component} from "angular2/core";
import {OnInit} from "angular2/core";
import {Contact} from "./contact";
import {ContactService} from "./contact.service";
import {Router} from "angular2/router";
import {RouteParams} from "angular2/router";


@Component({
	selector: "new-contact",
	template: `
		<p>Create New Contact</p>
		First Name : <input type="text" #firstName ><br>
        Last Name : <input type="text" #lastName value="{{lastNameParam}}"><br>
        Age : <input type="number" #age><br>
        Email : <input type="text" #email>
        <button (click)="onAddContact(firstName.value, lastName.value, age.value, email.value)">Create</button>
	`
})

export class ContactCreatedComponent implements OnInit{
	public createdContact: {};
	public lastNameParam = "";
	private contactService: ContactService;
	private routeParams: RouteParams;

	constructor(contactService: ContactService, private router: Router, routeParams: RouteParams){
		this.contactService = contactService;
		this.routeParams = routeParams;
	}

	onAddContact(firstName, lastName, age, email){
		let contact: Contact = { firstName : firstName, lastName: lastName, age: age, email: email };

		this.contactService.insertContact(contact);
		this.router.navigate(['Contacts', 'NewContact']);
	}

	ngOnInit(): any{
		this.lastNameParam = this.routeParams.get('lastName');
	}
}