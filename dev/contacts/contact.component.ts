import {Component} from 'angular2/core';
import {Router} from 'angular2/router';
import {Contact} from './contact';

@Component({
	selector: "contact",
	template: `
        <p>Hello {{contact.firstName}} {{contact.lastName}}</p><br>
        First Name : <input [(ngModel)]="contact.firstName" type="text"><br>
        Last Name : <input [(ngModel)]="contact.lastName" type="text"><br>
        Age : <input [(ngModel)]="contact.age" type="number"><br>
        Email : <input [(ngModel)]="contact.email" type="text"><br>
        <button (click)="onAddContactWithLastName()">Create contact from this contact</button>
    `,
	inputs: ["contact"]
})

export class ContactComponent {
	public contact: Contact;
    public router: Router;

    constructor(router: Router){
        this.router = router;
    }

    onAddContactWithLastName() {
        this.router.navigate(['NewContact', { lastName: this.contact.lastName }]);
    }
}