export interface Contact {
	firstName: string,
	lastName: string,
	age: number,
	email: string
}