import {Component} from "angular2/core";
import {OnInit} from "angular2/core";
import {ContactComponent} from "./contact.component";
import {ContactService} from "./contact.service";
import {Contact} from "./contact";

@Component({
	selector: "contact-list",
	template: `
		<ul>
        	<li *ngFor="#contact of contacts"
        		(click)="onSelect(contact)"
        		[class.myApp]="selectedContact === contact">
		        {{contact.firstName}}
        	</li>
        </ul>
        <contact *ngIf="selectedContact != null" [contact]="selectedContact"></contact>
	`,
	directives: [ContactComponent],
	styleUrls: ["../src/css/app.css"]
})

export class ContactListComponent implements OnInit{
	public contacts: Contact[];
	public selectedContact=null;
	private contactService: ContactService;

	constructor(contactService: ContactService) {
		this.contactService = contactService;
	}

	getContacts() {
		this.contactService.getContacts().then((contacts: Contact[]) => this.contacts = contacts);
	}

	onSelect(contact) {
		this.selectedContact = contact;
	}

	ngOnInit(): any {
		this.getContacts();
	}

}