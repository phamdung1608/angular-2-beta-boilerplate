import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from "angular2/router";
import {RouteConfig} from "angular2/router";

import {ContactListComponent} from "./contacts/contact-list.component";
import {ContactCreatedComponent} from "./contacts/contact-created.component";

@Component({
    selector: 'my-app',
    template: `
    	<header>
    		<nav>
    			<a [routerLink]="['Contacts']">Contacts</a>
    			<a [routerLink]="['NewContact']">New Contact</a>
    		</nav>
    	</header>
        <div class="main">
        	<router-outlet></router-outlet>
        </div>
    `,
    styleUrls: ["../src/css/app.css"],
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
	{path: '/contacts', name: 'Contacts', component: ContactListComponent},
	{path: '/new-contact', name: 'NewContact', component: ContactCreatedComponent},
    {path: '/new-contact/:lastName', name: 'NewContactWithLastName', component: ContactCreatedComponent}
])
export class AppComponent {
	
}